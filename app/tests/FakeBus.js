class FakeBus{

    constructor(){
        this.lastTopic = undefined
        this.subscribedTopics = []
    }

    subscribe(topic){
        this.subscribedTopics.push(topic)
    }

    publish(topic){
        this.lastTopic = topic
    }

    lastCall(){
        return this.lastTopic
    }

    subscriptions(){
        return this.subscribedTopics
    }
    
    pushNotification(){
      return
    }

}
export default FakeBus
