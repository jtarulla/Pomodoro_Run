import Pomodoro from '../../js/services/Pomodoro.js'
import Time from '../../js/libraries/Time.js'
import Bus from '../../js/libraries/Bus.js'
import Callback from '../Callback.js'
import StubDate from '../StubDate.js'

describe('Pomodoro service', () => {
  let bus, time, date = null

  beforeEach(() => {
    bus = new Bus()
    date = new StubDate()
    time = new Time(date)

  })

  it('subscribes to start event', () => {
    spyOn(bus, 'subscribe')

    new Pomodoro(bus, time)

    expect(bus.subscribe).toHaveBeenCalledWith('timer.start', aCallback())
  })

  it('on break, publishes break event with initial time left', () => {
    const callback = new Callback(bus, 'timer.isAtBreak')
    const initialTimeLeftBreak = {
      isPaused: true,
      minutes: 5,
      seconds: 0,
      isAtBreak: true,
      isAtLongBreak: false,
      isAtSynchro: false,
      isAtRetro: false,
      counter: 0,
    }
    const service = new Pomodoro(bus, time)

    service.startInBreak()

    expect(callback.hasBeenCalledWith()).toEqual(initialTimeLeftBreak)
  })

  it('on long break, publishes long break event with initial time left', () => {
    const callback = new Callback(bus, 'timer.isAtLongBreak')
    const initialTimeLeftLongBreak = {
      isPaused: true,
      minutes: 15,
      seconds: 0,
      isAtBreak: false,
      isAtLongBreak: true,
      isAtSynchro: false,
      isAtRetro: false,
      counter: 0
    }
    const service = new Pomodoro(bus, time)

    service.startInLongBreak()

    expect(callback.hasBeenCalledWith()).toEqual(initialTimeLeftLongBreak)
  })


  it('does not send a negative time left', () => {
    const callback = new Callback(bus, 'timer.end')
    const zeroTimeLeft = {
      isPaused: false,
      minutes: 0,
      seconds: 0,
      isAtBreak: false,
      isAtLongBreak: false,
      isAtSynchro: false,
      isAtRetro: false,
      counter: 1
    }
    const service = new Pomodoro(bus, time)
    service.pushNotificationPomodoro = ""

    service.start()
    date.moveToFuture()

    service.calculateTimeLeft()

    expect(callback.hasBeenCalledWith()).toEqual(zeroTimeLeft)
  })


  it('allows countdown to start while pomodoro is in break', () => {
    const resumedTimeLeft = {
      isPaused: false,
      minutes: 4,
      seconds: 0,
      isAtBreak: true,
      isAtLongBreak: false,
      isAtSynchro: false,
      isAtRetro: false,
      counter: 0
    }
    const callback = new Callback(bus, 'timer.timeLeft')
    const service = new Pomodoro(bus, time)

    service.startInBreak()
    service.start()

    date.moveOneMinuteToFuture()
    service.calculateTimeLeft()
    expect(callback.hasBeenCalledWith()).toEqual(resumedTimeLeft)
  })

  describe('Counter', () => {

    it('when pomodoro starts the counter is 0', () => {
      const count = 0
      const service = new Pomodoro(bus, time)
      const result = service.counterPomodoro

      expect(result).toEqual(count)
    })

    it('When timer finish add one to counter', () => {
      const count = 1
      const service = new Pomodoro(bus, time)

      service.isAtBreak = false
      service.isAtLongBreak = false
      service.isAtSynchro = false
      service.isAtRetro = false,

      service.addOneToCounter()

      const result = service.counterPomodoro


      expect(result).toEqual(count)
    })

    it('At break, when timer finish dont add one to counter', () => {
      const count = 0
      const service = new Pomodoro(bus, time)

      service.isAtBreak = true

      service.addOneToCounter()

      const result = service.counterPomodoro


      expect(result).toEqual(count)
    })

    it('At long break, when timer finish dont add one to counter', () => {
      const count = 0
      const service = new Pomodoro(bus, time)

      service.isAtLongBreak = true

      service.addOneToCounter()

      const result = service.counterPomodoro


      expect(result).toEqual(count)
    })

    it('At long break, when timer finish dont add one to counter', () => {
      const count = 0
      const service = new Pomodoro(bus, time)

      service.isAtSynchro = true

      service.addOneToCounter()

      const result = service.counterPomodoro


      expect(result).toEqual(count)
    })
  })

  describe('Alarm', () => {
    it(' reproduces the alarm sound after the time ends', () => {
      const service = new Pomodoro(bus, time)
      spyOn(service, 'alarm')

      service.putTimeToZero()

      expect(service.alarm).toHaveBeenCalled()
    })
  })

  describe('Synchro', () => {

    it('on synchro, publishes synchro event with initial time left', () => {
      const callback = new Callback(bus, 'timer.isAtSynchro')
      const initialTimeLeftSynchro = {
        isPaused : true,
        minutes: 45 ,
        seconds: 0,
        isAtBreak: false,
        isAtLongBreak: false,
        isAtSynchro: true,
        isAtRetro: false,
        counter: 0,
      }
      const service = new Pomodoro(bus, time)

      service.startInSynchro()

      expect(callback.hasBeenCalledWith()).toEqual(initialTimeLeftSynchro)
    })

  })

  describe('Retro', () => {

    it('on retro, publishes retro event with initial time left', () => {
      const callback = new Callback(bus, 'timer.isAtRetro')
      const initialTimeLeftRetro = {
        minutes: 60 ,
        seconds: 0,
        isPaused: true,
        isAtBreak: false,
        isAtLongBreak: false,
        isAtSynchro: false,
        isAtRetro: true,

        counter: 0,
      }
      const service = new Pomodoro(bus, time)

      service.startInRetro()

      expect(callback.hasBeenCalledWith()).toEqual(initialTimeLeftRetro)
    })
  })

  function aCallback() {
    return jasmine.any(Function)
  }
})
