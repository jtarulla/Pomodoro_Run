import Translation from '../../js/services/Translations.js'
import TranslateService from '../../js/services/TranslateService.js'
import Callback from '../Callback.js'
import Bus from '../../js/libraries/Bus.js'


describe ('Translation Service',() => {

    it ('returns a translation collection', () => {
        let key = 'start'
        let translations = Translation.retrieve()
        let translated = 'Inicio'

        expect(translations[key]).toEqual(translated)
    })

    it ('publishes a translation event',() => {
        const bus = new Bus()
        const callback = new Callback(bus, 'get.translations')
        const collection = {
            start:'Inicio',
            break:'Descanso',
            longBreak: 'Descanso largo',
            reset:'Reinicio',
            synchro: 'Sincro',
            retro: 'Retro', 
            numberOfPomodoros:'Número de Pomodoros',
            pushNotificationPomodoro:'Has terminado el Pomodoro. Tómate un descanso',
            pushNotificationBreak: 'Tu descanso ha terminado. Vuelve al trabajo',

        }
        const service = new TranslateService(bus)

        service.sendCollection()

        expect(callback.hasBeenCalledWith()).toEqual(collection)
    })

})
