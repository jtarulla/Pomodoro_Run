const FUTURE_MOMENT = 10000000000000000
const MINUTE_MILLISECONDS = 60000
const INITIAL_MOMENT = 0

class StubDate {
  constructor(moment=INITIAL_MOMENT) {
    this.moment = moment
  }

  now() {
    return this.moment
  }

  moveToFuture() {
    this.moment += FUTURE_MOMENT
  }

  moveOneMinuteToFuture() {
    this.moment += MINUTE_MILLISECONDS
  }
}

export default StubDate
