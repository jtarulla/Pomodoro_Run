import PushNotification from '../../js/components/PushNotification.js'
import FakeBus from '../FakeBus.js'

describe('Notification Push', () => {
	let push, bus = null

	let collection, pushNotification = null

	beforeEach(() => {
		collection = {
			pushNotificationPomodoro: 'Pomodoro',
			pushNotificationBreak: 'Break',
		}
		push = {}
		push.create = () => {

		}
		bus = new FakeBus()
		pushNotification = new PushNotification(bus, push)
	})

	it('should pop up a notification when pomodoro countdown finishes', () => {

		const state = { isAtBreak: false}
		
		pushNotification.retrievePushTranslations(collection)
		pushNotification.typeMessage(state)

		const result = pushNotification.notificationMessage

		expect(result).toEqual('Pomodoro')
	})

	it('should pop up a notification when break countdown fineshes', () => {
		const state = { isAtBreak: true}
		
		pushNotification.retrievePushTranslations(collection)
		pushNotification.typeMessage(state)

		const result = pushNotification.notificationMessage

		expect(result).toEqual('Break')
	})

	it('should pop up a notification when long break countdown fineshes', () => {
		const state = { isAtLongBreak: true}
		
		pushNotification.retrievePushTranslations(collection)
		pushNotification.typeMessage(state)

		const result = pushNotification.notificationMessage

		expect(result).toEqual('Break')
	})

})