import PomodoroButtons from '../../js/components/PomodoroButtons.js'
import FakeBus from '../FakeBus.js'
import FakeDocument from '../FakeDocument.js'

describe("Pomodoro Buttons component", () => {
  let document, bus, container = null

  beforeEach(() => {
    document = new FakeDocument()
    bus = new FakeBus()
    container = {}
  })

  it("renders the start label", () => {
    const pomodoro = new PomodoroButtons(container, bus, document)
    const label = 'any string'
    const collection = {start: label}

    pomodoro.renderTranslate(collection)
    pomodoro.draw()

    expect(container.innerHTML).toContain(label)
  })


  it('asks the countdown to reset ', () => {
    const pomodoro = new PomodoroButtons(container, bus, document)

    pomodoro.reset()

    expect(bus.lastCall()).toEqual('timer.reset')
  })

  it('asks the countdown to take a break ', () => {
    const pomodoro = new PomodoroButtons(container, bus, document)

    pomodoro.break()

    expect(bus.lastCall()).toEqual('timer.break')

  })

  it('asks the countdown to take a long break ', () => {
    const pomodoro = new PomodoroButtons(container, bus, document)

    pomodoro.longBreak()

    expect(bus.lastCall()).toEqual('timer.longBreak')

  })

  it('asks the countdown to start a synchro', () => {
    const pomodoro = new PomodoroButtons(container, bus, document)

    pomodoro.synchro()

    expect(bus.lastCall()).toEqual('timer.synchro')

  })

  it('asks the countdown to start a retro', () => {
    const pomodoro = new PomodoroButtons(container, bus, document)

    pomodoro.retro()

    expect(bus.lastCall()).toEqual('timer.retro')

  })
})

describe('translations', () => {
  let container, bus, document = null

  beforeEach(() => {
    document = new FakeDocument()
    bus = new FakeBus()
    container = {}
  })

  it ('component subscribe to translation service event', () => {
    new PomodoroButtons(container, bus, document)

    expect(bus.subscriptions()).toContain('get.translations')

  })
})
