import Bus from '../../js/libraries/Bus.js'
import Spy from './Spy.js'

describe('Bus service', () => {
  it('can publish a topic without subscriptions', () => {
    const bus = new Bus()
    const aTopic = 'this.is.a.topic'
    const aMessage = 'this is a message'

    expect(() => {
      bus.publish(aTopic, aMessage)
    }).not.toThrow(TypeError)
  })

  it('executes all the actions subscribed to a topic when it is published with a message', () => {
    const payload = 'Any message'
    const sameTopic = 'a.topic'
    const bus = new Bus()
    const spy = new Spy()


    bus.subscribe(sameTopic, spy.callback.bind(spy))
    bus.publish(sameTopic, payload)

    expect(spy.lastMessage()).toEqual(payload)
  })

})
