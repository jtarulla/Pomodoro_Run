import Time from '../../js/libraries/Time.js'
import StubDate from '../StubDate.js'

describe('Time', () => {
  const millisecondsInASecond = 1000
  const secondsInAMinute = 60

  it('gives the current moment into milliseconds', () => {
    const date = new StubDate(millisecondsInASecond)
    const time = new Time(date)

    const currentMoment = time.currentMoment()

    expect(currentMoment).toEqual(millisecondsInASecond)
  })

  it('transforms milliseconds into minutes', () => {
    const minutes = 2
    const milliseconds = minutes * secondsInAMinute * millisecondsInASecond
    const time = new Time()

    const result = time.toMinutes(milliseconds)

    expect(result).toEqual(minutes)
  })

  it('give us seconds passed in the current minute', () => {
    const minutes = 2
    const extraSeconds = 1
    const milliseconds = (minutes * secondsInAMinute * millisecondsInASecond) + (extraSeconds * millisecondsInASecond)
    const time = new Time()

    const resultSeconds = time.toSeconds(milliseconds)

    expect(resultSeconds).toEqual(extraSeconds)    
  })

  it('transforms milliseconds into minutes rounding down', () => {
    const minutes = 1
    const extraSeconds = 40
    const milliseconds = (minutes * secondsInAMinute * millisecondsInASecond) + (extraSeconds * millisecondsInASecond)
    const time = new Time()

    const result = time.toMinutes(milliseconds)

    expect(result).toEqual(minutes)
  })
})
