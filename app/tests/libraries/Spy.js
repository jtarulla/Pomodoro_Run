class Spy {
    constructor() {
      this.message = ''
    }

    callback(message) {
      this.message = message
    }

    lastMessage() {
      return this.message
    }
}  

export default Spy 

