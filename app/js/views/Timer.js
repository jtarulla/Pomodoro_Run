class Timer {
  constructor(document) {
    this.document = document
  }
  
  render(minutes, seconds) {
  
    return `
      <div id="container">
        <div id="timer">
          <div id="time">
            <span id="minutes">${this._format(minutes, seconds)}</span>
          </div>
          <div id="filler"></div>
        </div>
      </div>
    `
  }
  
  _format(minutes, seconds) {
    let minutesString = minutes.toString()
    let secondsString = seconds.toString()

    if (this._thereIsADigit(minutes)) { minutesString = `0${minutesString}` }
    if (this._thereIsADigit(seconds)) { secondsString = `0${secondsString}` }

    let result = `${minutesString}:${secondsString}`

    return result
  }

  _thereIsADigit(number) {
    return (number < 10)
  }
}

export default Timer
