class Counter {
  constructor(document) {
    this.document = document
    }

  render(properties) {

        return `
        <div>
            <span id="counterlabel" class="counterlabel">${this.labelCounter}</span>
            <span id="counterbox" class="counterbox">${properties}</span>
        </div>

        `
    }

  drawLabelCounter(collection){
    this.labelCounter = collection.numberOfPomodoros
  }

}
export default Counter
