const DISABLED_BUTTON = ' '+'disabled'
const EMPTY_STRING = ''

class Buttons {
  constructor(document) {
    this.document = document
  }

  render(properties) {
    return `
      <div id="container">
        <div>
          ${this._startButton(properties, this.startLabel)}
          ${this._resetButton(properties, this.resetLabel)}
          ${this._breakButton(properties, this.breakLabel)}
          ${this._longBreakButton(properties, this.longBreakLabel)}          
        </div>

      </div>

      <div>
        ${this._synchroButton(properties, this.synchroLabel)}
        ${this._retroButton(properties, this.retroLabel)}
      </div>

    `

  }
  addCallbacks(callbacks) {
    this._addOnClickToStart(callbacks.startCountDown)
    this._addOnClickToReset(callbacks.reset)
    this._addOnClickToBreak(callbacks.break)
    this._addOnClickToLongBreak(callbacks.longBreak)
    this._addOnClickToSynchro(callbacks.synchro)
    this._addOnClickToRetro(callbacks.retro)


  }

  _startButton(properties,action) {
    if (properties.isStartEnabled) { return this.buttonsRender('start', action, EMPTY_STRING)}

    return  this.buttonsRender('start',action, DISABLED_BUTTON)
  }  

  _resetButton(properties, action) {
    if (properties.isResetEnabled) { return this.buttonsRender('reset', action, EMPTY_STRING)}

    return this.buttonsRender('reset',action,  DISABLED_BUTTON)
  }

  _breakButton(properties,action) {
    if (properties.isBreakEnabled) { return this.buttonsRender('break', action, EMPTY_STRING)}

    return this.buttonsRender('break',action, DISABLED_BUTTON)
  }

  _longBreakButton(properties,action) {
    if (properties.isLongBreakEnabled) { return this.buttonsRender('longBreak', action, EMPTY_STRING)}

    return this.buttonsRender('longBreak',action, DISABLED_BUTTON)
  }

  _synchroButton(properties, action) {
    if (properties.isSynchroEnabled) {return this.buttonsRender('synchro', action, EMPTY_STRING)}

    return this.buttonsRender('synchro', action, DISABLED_BUTTON)
  } 

  _retroButton(properties, action) {
    if (properties.isRetroEnabled) {return this.buttonsRender('retro', action, EMPTY_STRING)}

    return this.buttonsRender('retro', action, DISABLED_BUTTON)
  } 

  _addOnClickToBreak(callback) {
    this._addOnClickTo('#break', callback)
  }

  _addOnClickToLongBreak(callback) {
    this._addOnClickTo('#longBreak', callback)
  }

  _addOnClickToStart(callback) {
    this._addOnClickTo('#start', callback)
  }

  _addOnClickToReset(callback) {
    this._addOnClickTo('#reset', callback)
  }

  _addOnClickToSynchro(callback) {
    this._addOnClickTo('#synchro', callback)
  }

  _addOnClickToRetro(callback) {
    this._addOnClickTo('#retro', callback)
  }

  _addOnClickTo(id, callback) {
    const element = this.document.querySelector(id)
    element.onclick = callback
  }

  buttonsRender(button,label, disabled){
   return `<button id="${button}" class="${button}"${disabled}>${label}</button>`

  }

  drawTranslations(collection){
    this.startLabel = collection.start
    this.resetLabel = collection.reset
    this.breakLabel = collection.break
    this.longBreakLabel = collection.longBreak
    this.synchroLabel = collection.synchro
    this.retroLabel = collection.retro

  }

}

export default Buttons
