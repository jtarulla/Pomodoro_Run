import PomodoroButtons from './components/PomodoroButtons.js'
import PomodoroTimer from './components/PomodoroTimer.js'
import PomodoroCounter from './components/PomodoroCounter.js'
import TranslateService from './services/TranslateService.js'
import PushNotification from './components/PushNotification.js'
//import Requester from './services/Requester.js'

import PomodoroService from './services/Pomodoro.js'
import Time from './libraries/Time.js'
import Bus from './libraries/Bus.js'
import Push from '../tests/vendor/push.min.js'

const time = new Time(Date)
const bus = new Bus()
const push = Push
//const requester = new Requester
const background = document.querySelector('#background')

const containerButtons = document.querySelector('#pomodoro-buttons')
const containerTimer = document.querySelector('#pomodoro-timer')
const containerCounter = document.querySelector('#pomodoro-counter')

new PomodoroService(bus, time)
new TranslateService(bus)
new PushNotification(bus, push)

const pomodoroButtons = new PomodoroButtons(containerButtons, bus, document, background)
pomodoroButtons.draw()

const pomodoroTimer = new PomodoroTimer(containerTimer, bus, document)
pomodoroTimer.draw()

const pomodoroCounter = new PomodoroCounter(containerCounter, bus, document)
pomodoroCounter.draw()
