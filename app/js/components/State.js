class State{
    constructor(){
        this.isStartEnabled = true
        this.isResetEnabled = false
        this.isBreakEnabled = true
        this.isLongBreakEnabled = true
        this.isSynchroEnabled = true
        this.isRetroEnabled = true

    }

    initialStatus(){
        this.isStartEnabled = true
        this.isResetEnabled = false
        this.isBreakEnabled = true
        this.isLongBreakEnabled = true
        this.isSynchroEnabled = true
        this.isRetroEnabled = true

    }

    runNonstopCountdown(){
        this.isStartEnabled = false
        this.isResetEnabled = false
        this.isBreakEnabled = false
        this.isLongBreakEnabled = false
        this.isSynchroEnabled = false
        this.isRetroEnabled = false

    }

    startMode(){
        this.isStartEnabled = false
        this.isResetEnabled = true
        this.isBreakEnabled = false
        this.isLongBreakEnabled = false
        this.isSynchroEnabled = true
        this.isRetroEnabled = true

    }

    resetMode(){
        this.isStartEnabled = true
        this.isResetEnabled = false
        this.isBreakEnabled = false
        this.isLongBreakEnabled = false
        this.isSynchroEnabled = false
        this.isRetroEnabled = false

    }

    breakMode(){
        this.isStartsdEnabled = true
        this.isResetEnabled = false
        this.isBreakEnabled = false
        this.isLongBreakEnabled = false
        this.isSynchroEnabled = false
        this.isRetroEnabled = false

    }

    longBreakMode(){
        this.isStartEnabled = true
        this.isResetEnabled = false
        this.isBreakEnabled = false
        this.isLongBreakEnabled = false
        this.isSynchroEnabled = false
        this.isRetroEnabled = false

    }

    synchroMode() {
        this.isStartEnabled = true
        this.isResetEnabled = false
        this.isBreakEnabled = false
        this.isLongBreakEnabled = false
        this.isSynchroEnabled = false
        this.isRetroEnabled = false

    }
    retroMode() {
        this.isStartEnabled = true
        this.isResetEnabled = false
        this.isBreakEnabled = false
        this.isLongBreakEnabled = false
        this.isSynchroEnabled = false
        this.isRetroEnabled = false

    }
}
export default State
