import Timer from '../views/Timer.js'

const pollingFrecuence = 950
const DEFAULT_PROPERTIES = { minutes: 0, seconds: 0 }

class PomodoroTimer {
  constructor(container, bus, document) {
    this.container = container
    this.renderer = new Timer(document)
    this.bus = bus
    this.timeOut = null

    this._subscribe()
  }

  _subscribe() {
    this.bus.subscribe('timer.started', this._showCountDown.bind(this))
    this.bus.subscribe('timer.isAtBreak', this._showCountDown.bind(this))
    this.bus.subscribe('timer.isAtLongBreak', this._showCountDown.bind(this))
    this.bus.subscribe('timer.isAtSynchro', this._showCountDown.bind(this))
    this.bus.subscribe('timer.isAtRetro', this._showCountDown.bind(this))
    this.bus.subscribe('timer.timeLeft', this._showCountDown.bind(this))
    this.bus.subscribe('timer.reseted', this._showCountDown.bind(this))
    this.bus.subscribe('timer.end', this._finalCountdown.bind(this))
  }

  _finalCountdown() {
    clearTimeout(this.timeOut)
  }

  draw() {
    this._drawRenderer(DEFAULT_PROPERTIES)
  }

  _showCountDown(message) {
    this._askCountDown()
    this._drawRenderer(message)
  }

  _askCountDown() {
    clearTimeout(this.timeOut)

    this.timeOut = setTimeout(() => {
      this.bus.publish('timer.askTimeLeft')
    }, pollingFrecuence)
  }

  _drawRenderer({minutes, seconds}) {
    this.container.innerHTML = this.renderer.render(minutes, seconds)
  }
}

export default PomodoroTimer
