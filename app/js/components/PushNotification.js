class PushNotification {

    constructor(bus, push){
        this.bus = bus
        this.push = push

        this.notificationMessage = null
        this.pushNotificationPomodoro = null
        this.pushNotificationBreak = null

        this._suscribe()
    }

    _suscribe(){
        this.bus.subscribe('timer.end', this.typeMessage.bind(this) )
        this.bus.subscribe('get.translations', this.retrievePushTranslations.bind(this))
    }

    typeMessage({isAtBreak, isAtLongBreak}){
        if(isAtBreak || isAtLongBreak){
          this.notificationMessage = this.pushNotificationBreak
        }else{
          this.notificationMessage = this.pushNotificationPomodoro
        }
        this.push.create(this.notificationMessage)
      }
    
    retrievePushTranslations(collection){
        this.pushNotificationPomodoro = collection.pushNotificationPomodoro
        this.pushNotificationBreak = collection.pushNotificationBreak
    }

}

export default PushNotification