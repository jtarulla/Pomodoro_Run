import Counter from '../views/Counter.js'

let DEFAULT_PROPERTIES = {counter: 0}

class PomodoroCounter{
  constructor(container, bus, document){
    this.container = container
    this.renderer = new Counter(document)
    this.bus = bus
    this._subscribe()

  }

  _subscribe() {
    this.bus.subscribe('get.translations', this.renderLabelCounter.bind(this))
    this.bus.subscribe('timer.end', this.draw.bind(this))
  }

  draw(message = DEFAULT_PROPERTIES){
    this.bus.publish('got.translations')
    this._drawRenderer(message)
  }

  renderLabelCounter(collection){
    this.collection = collection
    this.renderer.drawLabelCounter(this.collection)
  }


  _drawRenderer({counter} ){
    this.container.innerHTML = this.renderer.render(counter)
  }

}
export default PomodoroCounter
