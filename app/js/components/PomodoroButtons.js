import Buttons from '../views/Buttons.js'
import State from './State.js'

const SYNCHRO_COLOR = '#8e50ed'
const RETRO_COLOR = '#3541D4'
const DEFAULT_COLOR = '#479eeb'



class PomodoroButtons {
  constructor(container, bus, document, background) {
    this.state = new State
    this.container = container
    this.renderer = new Buttons(document)
    this.bus = bus
    this.containerBackground = background
    this._subscribe()
  }

  _subscribe() {
    this.bus.subscribe('get.translations', this.renderTranslate.bind(this))
    this.bus.subscribe('timer.started', this._startButton.bind(this))
    this.bus.subscribe('timer.isAtBreak', this._breakButton.bind(this))
    this.bus.subscribe('timer.isAtLongBreak', this._longBreakButton.bind(this))
    this.bus.subscribe('timer.reseted', this._resetButton.bind(this))
    this.bus.subscribe('timer.end', this._end.bind(this))
    this.bus.subscribe('timer.isAtSynchro', this._synchroButton.bind(this))
    this.bus.subscribe('timer.isAtRetro', this._retroButton.bind(this))

  }

  draw() {
    this.bus.publish('got.translations')
    this._drawRenderer(this.state)
  }


  reset() {
    this.bus.publish('timer.reset')
  }

  break() {
    this.bus.publish('timer.break')
  }

  longBreak() {
    this.bus.publish('timer.longBreak')
  }

  synchro() {
    this.bus.publish('timer.synchro')
  }

  retro() {
    this.bus.publish('timer.retro')
  }

  _start() {
    this.bus.publish('timer.start')
  }

  renderTranslate(collection){
    this.collection = collection
    this.renderer.drawTranslations(this.collection)
  }

  _startButton(countdown) {
    if(!countdown.isAtSynchro && !countdown.isAtRetro){
      this._changeBackgroundColor(DEFAULT_COLOR)
    }

    if(this._isAtSynchroTime(countdown) || this._isAtBreakTime(countdown)) {
      this.state.runNonstopCountdown()

    }else{
      this.state.startMode()
    }

    this.draw()
  }

  _isAtBreakTime(countdown){
    return countdown.isAtBreak || countdown.isAtLongBreak

  }

  _isAtSynchroTime(countdown) {
    return countdown.isAtSynchro || countdown.isAtRetro
  }


  _resetButton(){
    this.state.resetMode()

    this.draw()
  }

  _breakButton() {
    this.state.breakMode()

    this.draw()
  }

  _longBreakButton() {
    this.state.longBreakMode()

    this.draw()
  }

  _synchroButton(){
    this.state.synchroMode()
    this._changeBackgroundColor(SYNCHRO_COLOR)
    this.draw()
  }

  _retroButton(){
    this.state.retroMode()
    this._changeBackgroundColor(RETRO_COLOR)
    this.draw()
  }

  _changeBackgroundColor(color) {
    this.containerBackground.style.background = color
  }

  _end(){
    this.state.initialStatus()
    this.draw()
  }

  _drawRenderer(message) {
    const callbacks = {
      startCountDown: this._start.bind(this),
      reset: this.reset.bind(this),
      break: this.break.bind(this),
      longBreak: this.longBreak.bind(this),
      synchro: this.synchro.bind(this),
      retro: this.retro.bind(this)
    }

    this.container.innerHTML = this.renderer.render(message)
    this.renderer.addCallbacks(callbacks)
  }
}

export default PomodoroButtons
