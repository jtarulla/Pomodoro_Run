class Time {
  constructor(date) {
    this.date = date
  }

  currentMoment() {
    const inMilliseconds = this.date.now()
    return inMilliseconds
  }

  toSeconds(milliseconds){
    let seconds = new Date(milliseconds)
    return seconds.getSeconds()    
  }

  toMinutes(milliseconds) {
    let minutes = new Date(milliseconds)
    return minutes.getMinutes()


  }
}

export default Time
