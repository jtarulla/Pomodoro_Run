import Translations from './Translations.js'

class TranslateService {
	constructor(bus){
		this.bus = bus
		this.bus.subscribe('got.translations', this.sendCollection.bind(this))
	}
	sendCollection() {
		const collection = Translations.retrieve()
		this.bus.publish('get.translations',collection)
	}
}

export default TranslateService
