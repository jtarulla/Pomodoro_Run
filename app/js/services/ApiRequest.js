const TO_MILLISECONDS = 60 * 1000

class ApiRequest {

	static async getJson(route){
		const api_url = `http://127.0.0.1:8081/${route}`
		let response = await fetch(api_url)
		return await response.json()
		// let {time} = await response.json()
		// return time * TO_MILLISECONDS
	}

	static async postJson(route){
		const api_url = `/${route}`
		let response = await fetch(api_url)
	}
}

export default ApiRequest
