import ApiRequest from './ApiRequest.js'

const NOT_STARTED = 0
const NO_TIME_LEFT = 0
const NOT_PAUSED = 0
const INITIAL_BREAK_TIME_LEFT = 5 * 60 * 1000
const INITIAL_LONG_BREAK_TIME_LEFT = 15 * 60 * 1000
const INITIAL_SYNCHRO_TIME_LEFT = 45 * 60 * 1000
const INITIAL_RETRO_TIME_LEFT = 60 * 60 * 1000
const INITIAL_COUNTER = 0
const INITIAL_TIME =  25 * 60 * 1000

class Pomodoro {

  constructor(bus, time) {
    this.time = time
    this.bus = bus

    this.initial_time = null

    this.timeLeft = NO_TIME_LEFT

    this.startedAt = NOT_STARTED
    this.pausedAt = NOT_PAUSED

    this.isPaused = false
    this.isAtBreak = false
    this.isAtLongBreak = false
    this.isAtSynchro = false
    this.isAtRetro = false

    this.isReseted = false
    this.initialBreak = false
    this.initialLongBreak = false

    this.counterPomodoro = INITIAL_COUNTER

    this._subscribe()
    this.getInitialTime()
  }

  _subscribe() {
    this.bus.subscribe('timer.start', this.start.bind(this))
    this.bus.subscribe('timer.break', this.startInBreak.bind(this))
    this.bus.subscribe('timer.longBreak', this.startInLongBreak.bind(this))
    this.bus.subscribe('timer.askTimeLeft', this.calculateTimeLeft.bind(this))
    this.bus.subscribe('timer.pause', this.pause.bind(this))
    this.bus.subscribe('timer.reset', this.reset.bind(this))
    this.bus.subscribe('timer.synchro', this.startInSynchro.bind(this))
    this.bus.subscribe('timer.retro', this.startInRetro.bind(this))
  }

  async getInitialTime(){
    const response = await ApiRequest.getJson('initialTime')
    console.log(response)
    this.initial_time = response.time * 60 * 1000

  }

  initalTime() {
    let callback = this.buildCallback('initial')
    let url = '/initialTime'
    this.client.hit(url, callback)
  }

  start() {

    if(this._pomodoroNotStarted()) {
      this._intialStatus()
    }

    if(this.isPaused) {
      this._startInPause()
    }


    if(this.isAtBreak) {
      this.startInBreak()
    }

    if(this.isAtLongBreak) {
      this.startInLongBreak()
    }

    if(this.isAtSynchro) {
      this.startInSynchro()
    }
    if(this.isAtRetro){
      this.startInRetro()
    }

    this._notPaused()

    this.bus.publish('timer.started', this._message())
  }

  _notPaused() {
    this.isPaused = false
  }


  _intialStatus() {
    this.timeLeft = this.initial_time
    this.startedAt = this._currentMoment()
    this.isPaused = false
    this.isAtBreak = false
    this.isReseted = false
    this.isAtLongBreak = false
    this.isAtSynchro = false
    this.isAtRetro = false
  }

  _pomodoroNotStarted(){
    return (!this.isPaused && !this.isAtBreak) || this.isReseted

  }

  _startInPause() {
    this.isPaused = false
    this.startedAt = this.startedAt + this._passedTimeFrom(this.pausedAt)
  }

  pause() {
    this.isPaused = true
    this.pausedAt = this._currentMoment()
    this.bus.publish('timer.paused', this._message())
  }

  startInBreak() {
    this.isAtBreak = true
    this.isAtLongBreak = false
    this.timeLeft = INITIAL_BREAK_TIME_LEFT
    this.isPaused = true
    this.startedAt = this._currentMoment()
    this.initialBreak = false

    this.bus.publish('timer.isAtBreak', this._message())
  }

  startInLongBreak() {
    this.isAtBreak = false
    this.isAtLongBreak = true
    this.timeLeft = INITIAL_LONG_BREAK_TIME_LEFT
    this.isPaused = true
    this.startedAt = this._currentMoment()
    this.initialBreak = false

    this.bus.publish('timer.isAtLongBreak', this._message())
  }

  startInSynchro() {
    this.isAtBreak = false
    this.isAtLongBreak = false
    this.timeLeft = INITIAL_SYNCHRO_TIME_LEFT
    this.isPaused = true
    this.startedAt = this._currentMoment()
    this.initialBreak = false
    this.isAtSynchro = true
    this.isAtRetro = false

    this.bus.publish('timer.isAtSynchro', this._message())
  }

  startInRetro() {
    this.isAtBreak = false
    this.isAtLongBreak = false
    this.timeLeft = INITIAL_RETRO_TIME_LEFT
    this.isPaused = true
    this.startedAt = this._currentMoment()
    this.initialBreak = false
    this.isAtSynchro = false
    this.isAtRetro = true

    this.bus.publish('timer.isAtRetro', this._message())
  }

  reset() {
    this.timeLeft = this.initial_time
    this.isPaused = true
    this.isReseted = true

    this.bus.publish('timer.reseted', this._message())
  }

  _calculateBreak(initial, state){
    if(initial) {
      this._stipulateBreakTime()
    }

    if(!initial && state) {
      this._isRunning(INITIAL_BREAK_TIME_LEFT, this.startedAt)
    }
  }

  _calculateLongBreak(initial, state){
    if(initial) {
      this._stipulateLongBreakTime()
    }

    if(!initial && state) {
      this._isRunning(INITIAL_LONG_BREAK_TIME_LEFT, this.startedAt)
    }
  }

  _calculateSynchro(initial, state){
    if(initial) {
      this._stipulateSynchroTime()
    }

    if(!initial && state) {
      this._isRunning(INITIAL_SYNCHRO_TIME_LEFT, this.startedAt)
    }
  }

  _calculateRetro(initial, state){
    if(initial) {
      this._stipulateRetroTime()
    }

    if(!initial && state) {
      this._isRunning(INITIAL_RETRO_TIME_LEFT, this.startedAt)
    }
  }

  calculateTimeLeft() {
    if(!this.isPaused) {
      this._isRunning(this.initial_time, this.startedAt)
      this._calculateBreak(this.initialBreak, this.isAtBreak)
      this._calculateLongBreak(this.initialBreak, this.isAtLongBreak)
      this._calculateSynchro(this.initialBreak, this.isAtSynchro)
      this._calculateRetro(this.initialBreak, this.isAtRetro)
    }

    if(!this._hasTimeLeft()) {
      this.putTimeToZero()
      this.addOneToCounter()

      this.bus.publish('timer.end', this._message())
      this._changeToInitialState()
    }else{
      this.bus.publish('timer.timeLeft', this._message())
    }
  }

  addOneToCounter(){
    if(!this.isAtBreak && !this.isAtRetro && !this.isAtSynchro && !this.isAtLongBreak) {
      return this.counterPomodoro++
    }
  }

  _changeToInitialState(){
    this.startedAt = NOT_STARTED
    this.isAtBreak = false
    this.isAtLongBreak = false
    this.isAtSynchro = false
    this.isAtRetro = false
  }

  _stipulateBreakTime() {
    this.timeLeft = INITIAL_BREAK_TIME_LEFT
  }

  _stipulateLongBreakTime() {
    this.timeLeft = INITIAL_LONG_BREAK_TIME_LEFT
  }

  _stipulateSynchroTime() {
    this.timeLeft = INITIAL_SYNCHRO_TIME_LEFT
  }

  _stipulateRetroTime() {
    this.timeLeft = INITIAL_RETRO_TIME_LEFT
  }

  putTimeToZero() {
    this.timeLeft = NO_TIME_LEFT
    this.alarm()
  }

  _isRunning(start, state) {
    this.timeLeft = start - this._passedTimeFrom(state)
  }

  _message() {
    let min = this._toVisulize60()

    return {
      minutes: min,
      seconds:this.time.toSeconds(this.timeLeft),
      isPaused: this.isPaused,
      isAtBreak: this.isAtBreak,
      isAtLongBreak: this.isAtLongBreak,
      isAtSynchro: this.isAtSynchro,
      isAtRetro: this.isAtRetro,
      counter: this.counterPomodoro,
    }
  }

  _toVisulize60(){
    let min = null
    if(this.timeLeft === INITIAL_RETRO_TIME_LEFT){
      min = 60
    }else {
      min = this.time.toMinutes(this.timeLeft)
    }
    return min
  }

  _hasTimeLeft() {
    return (this.timeLeft > NO_TIME_LEFT)
  }

  _passedTimeFrom(moment) {
    const currentMoment = this._currentMoment()
    const passedTime = currentMoment - moment
    return passedTime
  }

  _currentMoment() {
    return this.time.currentMoment()
  }

  alarm() {
    //let audio = new Audio('/alarm_songs/R2D2_Audio.mp3')
    //audio.play()
  }

}

export default Pomodoro
