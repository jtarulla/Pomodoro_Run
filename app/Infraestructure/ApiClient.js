import aja from 'aja'

export let APIClient = {
  URL: "http://localhost:8081/api",

  hit: function (endpoint, data, action, error = () =>{}){
    let base_url = this.URL
    aja()
    .method('get')
    .body(data)
    .url(base_url + endpoint)
    .on('success', action)
    .on('500', error)
    .go()
    }
}