const START_BUTTON = '.start'
const RESET_BUTTON = '.reset'
const BREAK_BUTTON = '.break'
const LONG_BREAK_BUTTON = '.longBreak'
const SYNCHRO_BUTTON = '.synchro'
const RETRO_BUTTON = '.retro'
const TIME_FINISH = 25.55 * 60 * 1000
const FIVE_SECONDS= 5 * 1000

class Pomodoro {

  visitOurHomePage() {
    const base_url = Cypress.env('app_server')
    cy.visit(`${base_url}`)
  }

  clickStartButton() {
    cy.get(START_BUTTON).click()
  }

  clickResetButton() {
    cy.get(RESET_BUTTON).click()
  }

  clickBreakButton() {
    cy.get(BREAK_BUTTON).click()
  }

  clickLongBreakButton() {
    cy.get(LONG_BREAK_BUTTON).click()
  }

  clickSynchroButton() {
    cy.get(SYNCHRO_BUTTON).click()
  }

  clickRetroButton() {
    cy.get(RETRO_BUTTON).click()
  }


}

class SyncedPomodoro extends Pomodoro{
  constructor(){
    super()
    cy.clock()
  }
  skipFiveSeconds() {
    cy.tick(FIVE_SECONDS)
  }

  timeFinishes(){
    cy.tick(TIME_FINISH)
  }
}

module.exports = {Pomodoro, SyncedPomodoro}
