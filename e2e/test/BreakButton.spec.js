import {Pomodoro}  from './Pomodoro.js'

describe('Break Button', () => {
    let pomodoro
    beforeEach(() => {
       pomodoro = new Pomodoro()
       pomodoro.visitOurHomePage()

    })

    it('sets time to initial 5:00 after pressing it', () =>{
      
      pomodoro.clickBreakButton()

      cy.get('#minutes').contains('5:00')
    })

    it ("When break time is declared, the start button is the only one Enabled", ()=> {

      pomodoro.clickBreakButton()

      cy.get('.start').should('be.enabled')
      cy.get('.reset').should('be.disabled')
      cy.get('.break').should('be.disabled')
      cy.get('.longBreak').should('be.disabled')
    })

    it ("While break time is runnig, all buttons are disabled", ()=> {

      pomodoro.clickBreakButton()
      pomodoro.clickStartButton()

      cy.get('.start').should('be.disabled')
      cy.get('.reset').should('be.disabled')
      cy.get('.break').should('be.disabled')
      cy.get('.longBreak').should('be.disabled')
    })
  })
