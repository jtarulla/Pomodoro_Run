import {Pomodoro} from './Pomodoro.js'

describe('Retro Button', () => {
  let pomodoro 
  
  beforeEach(() => {
    pomodoro = new Pomodoro()
    pomodoro.visitOurHomePage()
  })

    it('sets time to initial retro time after pressing it', () =>{
  
      pomodoro.clickRetroButton()
  
      cy.get('#minutes').contains('60:00')
    })
  
    it('sets the background to Retro color state', () =>{
  
      pomodoro.clickRetroButton()
  
      cy.get('body').should('have.css', 'background-color', 'rgb(53, 65, 212)')
    })
  
    it ("While Retro time is runing, reset buttons is disabled", ()=> {
  
      pomodoro.clickRetroButton()
      pomodoro.clickStartButton()
    })
    
    it ("While Retro time is runing, all buttons are disabled", ()=> {
  
      pomodoro.clickRetroButton()
      pomodoro.clickStartButton()
  
      cy.get('.start').should('be.disabled')
      cy.get('.reset').should('be.disabled')
      cy.get('.break').should('be.disabled')
      cy.get('.longBreak').should('be.disabled')
    })
  })