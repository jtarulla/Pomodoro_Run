import {Pomodoro, SyncedPomodoro} from './Pomodoro.js'

describe("Start Button", ()=> {
  let pomodoro

  beforeEach(() => {
    pomodoro = new Pomodoro()
    pomodoro.visitOurHomePage()
  })

  it("when push start button the time starts at 25:00",() =>{

    pomodoro.clickStartButton()

    cy.get('#minutes').contains("25:00")
  })

  it("when a countdown starts shows time passes", ()=> {
    let synced = new SyncedPomodoro()

    synced.visitOurHomePage()
    synced.clickStartButton()
    synced.skipFiveSeconds()

    cy.get('#minutes').should("include.text", "24")
  })

  it ("when countdown starts, the Reset button is Enabled", ()=> {

    pomodoro.clickStartButton()

    cy.get('.reset').should('be.enabled')
  })

  it ("when countdown starts, the Break button is Disabled", ()=> {

    pomodoro.clickStartButton()

    cy.get('.break').should('be.disabled')
  })

  it ("when countdown starts, the Long Break button is Disabled", ()=> {

    pomodoro.clickStartButton()

    cy.get('.longBreak').should('be.disabled')
  })
})
