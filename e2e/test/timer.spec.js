import {Pomodoro, SyncedPomodoro} from './Pomodoro.js'

describe('Timer', () => {

    it('Time starts at  00:00', () =>{
      const pomodoro = new Pomodoro()

      pomodoro.visitOurHomePage()

      cy.get('#minutes').contains('00:00')


    })

    it('Time finishes at 00:00', () =>{
      const synced = new SyncedPomodoro()
      
      synced.visitOurHomePage()
      synced.clickStartButton()
  
      synced.timeFinishes()
  
      cy.get('#minutes').contains('00:00')
    })
  })
  