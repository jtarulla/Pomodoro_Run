import {Pomodoro} from './Pomodoro.js'

describe('Long Break Button', () => {
  let pomodoro 

  beforeEach(() => {
    pomodoro = new Pomodoro()
    pomodoro.visitOurHomePage()
  })

    it('sets time to initial 15:00 after pressing it', () =>{
  
      pomodoro.clickLongBreakButton()
  
      cy.get('#minutes').contains('15:00')
    })
  
    it ("When long break time is declared, the start button is the only one Enabled", ()=> {
  
      pomodoro.clickLongBreakButton()
  
      cy.get('.start').should('be.enabled')
      cy.get('.reset').should('be.disabled')
      cy.get('.break').should('be.disabled')
      cy.get('.longBreak').should('be.disabled')
    })
  
    it ("While long break time is runnig, all buttons are disabled", ()=> {
  
      pomodoro.clickLongBreakButton()
      pomodoro.clickStartButton()
  
      cy.get('.start').should('be.disabled')
      cy.get('.reset').should('be.disabled')
      cy.get('.break').should('be.disabled')
      cy.get('.longBreak').should('be.disabled')
    })
  })
  