import {Pomodoro} from './Pomodoro.js'

describe('Reset Button', () => {
  let pomodoro 
 
  beforeEach(() => {
    pomodoro = new Pomodoro()
    pomodoro.visitOurHomePage()
  })

  
    it('sets time to initial 25:00 after using reset button', () =>{
  
      pomodoro.clickStartButton()
      pomodoro.clickResetButton()
  
      cy.get('#minutes').contains('25:00')
    })
  
    it ("when the countdown is reset, the start button is the only one Enabled", ()=> {

      pomodoro.clickStartButton()
      pomodoro.clickResetButton()
  
      cy.get('.start').should('be.enabled')
      cy.get('.reset').should('be.disabled')
      cy.get('.break').should('be.disabled')
      cy.get('.longBreak').should('be.disabled')
    })
  
  })