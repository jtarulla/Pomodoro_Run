import {Pomodoro} from './Pomodoro.js'

describe('Synchro Button', () => {
  let pomodoro 
  
  beforeEach(() => {
    pomodoro = new Pomodoro()
    pomodoro.visitOurHomePage()
  })

    it('sets time to initial 45:00 after pressing it', () =>{
      
      pomodoro.clickSynchroButton()
  
      cy.get('#minutes').contains('45:00')
    })
  
    it('sets the background to Synchro color state', () =>{
      
      pomodoro.clickSynchroButton()
  
      cy.get('body').should('have.css', 'background-color', 'rgb(142, 80, 237)')
    })
  
    it ("While synchro time is runnig, reset buttons are disabled", ()=> {
     
      pomodoro.clickSynchroButton()
      pomodoro.clickStartButton()
    })
    
    it ("While Synchro time is runnig, all buttons are disabled", ()=> {
      
      pomodoro.clickSynchroButton()
      pomodoro.clickStartButton()
  
      cy.get('.start').should('be.disabled')
      cy.get('.reset').should('be.disabled')
      cy.get('.break').should('be.disabled')
      cy.get('.longBreak').should('be.disabled')
    })
  })