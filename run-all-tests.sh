#/bin/sh

docker-compose exec -T app npm run test
docker run --network="host" -v $(pwd)/e2e:/workdir rdelafuente/cypress
