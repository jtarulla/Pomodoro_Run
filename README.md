# Pomodoro Devscola

Project originally created as a tool to learn Javascript for the "Run FZBZ" attendants at Devscola. 

The final goal is to provide an app where users can share the same pomodoro session. 

## System requirements

  - Firefox version 60 or higher
  - docker-compose
  - Cypress (only for access to Cypress app)


## How to run the application

In your terminal type the following commands:
  - `docker-compose up --build`

Copy  the url server that is now written in your console and paste it in your Firefox browser.


## How to run the unitary tests

Test will run in 'localhost:8080/tests.html'

## Running all the tests

After running the docker, execute:

  - `sh run-all-tests.sh`

## How to run the integrity tests (requires Cypress installation)
In your console type the following command:
  - npm run test-all (in order to launch tests in terminal).
  - node_modules/.bin/cypress open (in order to access Cypress app).
  - node_modules/.bin/cypress run (in order to run the tests).

## Updating changes on Heroku

After commiting and pushing changes on git, type the command below:

- `git push heroku master --force`

## Deploying to Demo

Currently, we have a machine in Heroku to deploy the application for demo purposes, you can check it out at:

  - https://app-squadpomodoro.herokuapp.com/