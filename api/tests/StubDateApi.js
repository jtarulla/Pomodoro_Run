const FUTURE_MOMENT = 10000000000000000
const MINUTE_MILLISECONDS = 60000
const INITIAL_MOMENT = 0

function now (){
    return INITIAL_MOMENT
}

function endTime(){
    return FUTURE_MOMENT
}

function moveOneMinuteForward(){
    return MINUTE_MILLISECONDS

}
  module.exports = { now, endTime, moveOneMinuteForward }
