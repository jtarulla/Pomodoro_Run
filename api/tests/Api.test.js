const retrieveInitialTime = require('./retrieveInitialTime.js')
const { now, endTime, moveOneMinuteForward } = require('StubDateApi')
 
describe ('practice',()=>{   
  
    it("returns undefined by default", () => {
        const mock = jest.fn()
      
        let result = mock("foo")
      
        expect(result).toBeUndefined()
        expect(mock).toHaveBeenCalled()
        expect(mock).toHaveBeenCalledTimes(1)
        expect(mock).toHaveBeenCalledWith("foo")
      })
    })

describe ('Initial time response',()=>{   
    it('returns 25 minutes', ()=> {

    const result = {time:'25'}

    const expected = retrieveInitialTime()

    expect(expected).toEqual(result)
    })
})

describe ('Time testing', ()=>{
    it('runs seconds in server', ()=>{


    expect(1).toEqual(1)
    })

})