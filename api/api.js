const express = require('express')
const cors = require('cors')
const path = require('path')
const api = express()
const port = process.env.PORT || 8081
const retrieveInitialTime = require('./tests/retrieveInitialTime.js')

api.use(cors())

api.get('/initialTime',(req,res)=>{
   res.json(retrieveInitialTime())
})

api.listen(port, () => console.log(`Listening on port ${port}!`))
